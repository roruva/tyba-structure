import { combineEpics } from "redux-observable";

import authEpics from "./auth/auth.epics";
import weatherEpics from "./weather/weather.epics";

export default combineEpics(...authEpics, ...weatherEpics);
