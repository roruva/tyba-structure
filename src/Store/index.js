import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";

// Reducers
import reducer from "./reducer";
//   Epics
import epics from "./epics";

// Epic middleware
const epicMiddleware = createEpicMiddleware();

// Middlewares
const middlewares = [epicMiddleware];

const store = createStore(reducer, applyMiddleware(...middlewares));

epicMiddleware.run(epics);

export default store;
