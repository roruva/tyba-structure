import { WeatherTypes } from "./weather.actions.type";

export const getWeatherByCity = cityName => ({
  cityName,
  type: WeatherTypes.GET_WEATHER_BY_CITY
});

export const getWeatherByCitySuccess = weather => ({
  weather,
  type: WeatherTypes.GET_WEATHER_BY_CITY_SUCCESS
});

export const weatherError = error => ({
  error,
  type: WeatherTypes.WEATHER_ERROR
});
