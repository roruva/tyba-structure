import { ofType } from "redux-observable";
import { ajax } from "rxjs/ajax";
import { catchError, map, mergeMap } from "rxjs/operators";
import { of } from "rxjs";

import { WeatherTypes } from "./weather.actions.type";
import { getWeatherByCitySuccess } from "./";

const getWeatherByCityName = action$ => {
  return action$.pipe(
    ofType(WeatherTypes.GET_WEATHER_BY_CITY),
    mergeMap(({ cityName }) => {
      return ajax
        .getJSON(
          `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=8d671d088abd814a3632b7f6c5f36c47&units=metric`
        )
        .pipe(
          map(weather => getWeatherByCitySuccess(weather)),
          catchError(error => {
            console.error("getWeatherByCityName", error);
            return of(error);
          })
        );
    })
  );
};

export default [getWeatherByCityName];
