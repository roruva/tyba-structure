import { WeatherTypes } from "./weather.actions.type";

const initialState = {
  loading: false,
  weather: undefined,
  error: undefined
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case WeatherTypes.GET_WEATHER_BY_CITY: {
      return { ...state, loading: true };
    }
    case WeatherTypes.GET_WEATHER_BY_CITY_SUCCESS: {
      const { weather } = action;
      return { ...state, loading: false, weather };
    }
    case WeatherTypes.WEATHER_ERROR: {
      const { error } = action;
      return { ...state, loading: false, error };
    }
    default:
      return state;
  }
}
