import { combineReducers } from "redux";

import authReducer from "./auth/auth.reducer";
import weatherReducer from "./weather/weather.reducer";

export default (rootReducer = combineReducers({
  auth: authReducer,
  weather: weatherReducer
}));
