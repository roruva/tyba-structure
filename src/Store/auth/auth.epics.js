import { ofType } from "redux-observable";
import { ajax } from "rxjs/ajax";
import { catchError, map, mergeMap } from "rxjs/operators";

import { AuthTypes } from "./auth.actions.type";

const logIn = action$ => {
  return action$.pipe(
    ofType(AuthTypes.LOGIN),
    mergeMap(({ email, password }) => {
      return ajax.post("somewhere", { email, password }).pipe(
        map(response => console.log("response", response)),
        catchError(error => console.error("logIn", error))
      );
    })
  );
};

export default [logIn];
