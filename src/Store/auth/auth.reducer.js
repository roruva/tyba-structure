import { AuthTypes } from "./auth.actions.type";

const initialState = {
  loading: false
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case AuthTypes.LOGIN: {
      return { ...state, loading: true };
    }
    default:
      return state;
  }
}
