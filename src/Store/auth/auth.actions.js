import { AuthTypes } from "./auth.actions.type";

export const logIn = (email, password) => ({
  email,
  password,
  type: AuthTypes.LOGIN
});
