import { createStackNavigator } from "react-navigation";

// Screens
import { Home } from "../Screens/DashboardModule";

// Main Stack
const DashboardStackNavigation = createStackNavigator(
  {
    Home
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      header: null
    }
  }
);

export default DashboardStackNavigation;
