import { createAppContainer, createSwitchNavigator } from "react-navigation";

// Modules
import DashboardStackNavigation from "./Dashboard.Module.Stack";

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      Dashboard: DashboardStackNavigation
    },
    {
      initialRouteName: "Dashboard"
    }
  )
);

export default AppContainer;
